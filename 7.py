def fib(n):
	if n >= 2:
		return fib(n - 1) + fib(n - 2);
	if n < 0:
		return (-1) ** (1 - n) * fib(-n)
	return 0 if n == 0 else 1;

print(fib(int(input("Enter fib number:"))));