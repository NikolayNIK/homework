def func1(a):
	return map(lambda a: 2 * a, a)

def func2(a, b, c):
	return map(lambda a, b, c: a * b * c, a, b, c)

def func3(a):
	return map(lambda a: len(a), a)

def func4(a):
	return filter(lambda a: a % 2 == 0, a)

def func5(a):
	return filter(lambda a: a, a) # aaaaaaaaa

def func6(a, b, c):
	return zip(a, b, c);

def func6(a, b):
	return zip(a, map(lambda a: 2 * a, b));