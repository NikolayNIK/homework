f = open("input.txt", "r")
cols = tuple(map(int, f))
f.close()

total = 0
for i in range(1, len(cols) - 1):
	left = 0
	for j in range(i, -1, -1):
		if cols[j] > left:
			left = cols[j]
	
	right = 0
	for j in range(i, len(cols)):
		if cols[j] > right:
			right = cols[j]
	
	a = min(left, right) - cols[i]
	if a > 0:
		total += a

print(total)