class graph:
	def __init__(self, func, *args):
		self.func = func
		self.args = args
		self.i = -1;

	def __iter__(self):
		return self

	def __next__(self):
		self.i += 1
		
		buffer = "self.func("
		for arg in self.args:
			if self.i >= len(arg):
				raise StopIteration
			
			buffer += str(arg[self.i])
			buffer += ','
		return eval(buffer[:-1] + ')')

class tar:
	def __init__(self, *args):
		self.args = args
		self.i = -1
	
	def __iter__(self):
		return self
	
	def __next__(self):
		self.i += 1
		
		buffer = ""
		for arg in self.args:
			if self.i >= len(arg):
				raise StopIteration
			
			buffer += str(arg[self.i])
			buffer += ','
		return eval(buffer[:-1])

class sieve:
	def __init__(self, func, iterable):
		self.func = func
		self.iterator = iterable.__iter__()
		self.i = -1
	
	def __iter__(self):
		return self
	
	def __next__(self):
		while True:
			tmp = self.iterator.__next__()
			if self.func(tmp):
				return tmp