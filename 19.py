def fib(n):
	if n >= 2:
		return fib(n - 1) + fib(n - 2);
	if n < 0:
		return (-1) ** (1 - n) * fib(-n)
	return 0 if n == 0 else 1;

def min(*args):
	value = args[0] if len(args) > 0 else None;
	for i in args:
		if i < value:
			value = i;
	return value;

def nothing(a, *args, **kwargs):
	pass;

def min_capped(*args):
	value = 255;
	for i in args:
		if i < value and i >= 0:
			value = i;
	return value;