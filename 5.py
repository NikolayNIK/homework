print("ax^2 + bx + c = 0");
a = float(input("Enter a: "));
b = float(input("Enter b: "));
c = float(input("Enter c: "));

if a == 0:
    print("Single solution: ", -c / b);
else:
    d = b ** 2 - 4 * a * c;
    if d < 0:
        print("No solutions");
    else:
        rd = d ** (1/2);
        
        if d == 0:
            print("Two equal solutions: ", -b / (2 * a));
        else:
            print("Solution 1: ", (-b + rd) / (2 * a));
            print("Solution 2: ", (-b - rd) / (2 * a));
pass